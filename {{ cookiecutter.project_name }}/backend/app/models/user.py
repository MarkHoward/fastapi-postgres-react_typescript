"""User model."""
from sqlalchemy import BigInteger, Boolean, Column, Integer, String

from app.db.base_class import Base


class User(Base):
    """User model."""
    id = Column(Integer, primary_key=True, index=True)
    username = Column(String, index=True, nullable=False)
    full_name = Column(String)
    edipi = Column(BigInteger, index=True)
    hashed_password = Column(String, nullable=False)
    is_active = Column(Boolean(), default=True)
    is_superuser = Column(Boolean(), default=False)
