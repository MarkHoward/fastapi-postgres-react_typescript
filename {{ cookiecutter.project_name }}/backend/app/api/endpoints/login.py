"""Authentication endpoints."""
from typing import Any, Optional

from fastapi import APIRouter, Depends, Header, HTTPException
from fastapi.security import OAuth2PasswordRequestForm
from fastapi_jwt_auth import AuthJWT
from sqlalchemy.orm import Session

from app import crud, models, schemas
from app.api import deps

router = APIRouter()


@router.post("/login/password", response_model=schemas.Token)
def password_login(
    db: Session = Depends(deps.get_db),
    form_data: OAuth2PasswordRequestForm = Depends(),
    Authorize: AuthJWT = Depends(),
) -> Any:
    """OAuth2 compatible token login, get an access token for future requests."""
    user = crud.user.authenticate(
        db, username=form_data.username, password=form_data.password
    )
    if not user:
        raise HTTPException(status_code=400, detail="Incorrect username or password")
    elif not crud.user.is_active(user):
        raise HTTPException(status_code=400, detail="Inactive user")

    refresh_token = Authorize.create_refresh_token(subject=user.id)
    access_token = Authorize.create_access_token(
        subject=user.id, user_claims={"isSuperUser": user.is_superuser}
    )

    Authorize.set_refresh_cookies(refresh_token)
    return {
        "access_token": access_token,
        "token_type": "bearer",
    }


@router.post("/login/cac", response_model=schemas.Token)
def cac_login(
    *,
    db: Session = Depends(deps.get_db),
    Authorize: AuthJWT = Depends(),
    x_client_verified: Optional[str] = Header(None),
    x_subject_dn: Optional[str] = Header(None),
) -> Any:
    """Login with CAC credentials, get an access token for future requests."""
    if x_client_verified == "NONE":
        raise HTTPException(status_code=400, detail="CAC not found")

    try:
        edipi = x_subject_dn.split(",")[0].split(".")[-1]
    except KeyError:
        raise HTTPException(status_code=400, detail="Failed to extract EDIPI")

    user = crud.user.get_by_edipi(db, edipi=edipi)
    if user is None:
        raise HTTPException(status_code=400, detail="User not found for CAC")
    if not crud.user.is_active(user):
        raise HTTPException(status_code=400, detail="Inactive user")

    refresh_token = Authorize.create_refresh_token(subject=user.id)
    access_token = Authorize.create_access_token(
        subject=user.id, user_claims={"isSuperUser": user.is_superuser}
    )

    Authorize.set_refresh_cookies(refresh_token)
    return {
        "access_token": access_token,
        "token_type": "bearer",
    }


@router.post("/login/refresh", response_model=schemas.Token)
def refresh_access_token(
    db: Session = Depends(deps.get_db),
    Authorize: AuthJWT = Depends(),
) -> Any:
    """Use refresh token to get new access token."""
    Authorize.jwt_refresh_token_required()
    current_user_id = Authorize.get_jwt_subject()
    user = crud.user.get(db, id=current_user_id)

    new_access_token = Authorize.create_access_token(
        subject=current_user_id, user_claims={"isSuperUser": user.is_superuser}
    )
    return {
        "access_token": new_access_token,
        "token_type": "bearer",
    }


@router.post("/login/test-token", response_model=schemas.User)
def test_token(
    db: Session = Depends(deps.get_db),
    current_user: models.User = Depends(deps.get_current_user),
) -> Any:
    """Test access token."""
    return current_user


@router.get("/logout", response_model=schemas.Msg)
def logout(Authorize: AuthJWT = Depends()):
    """Logout and clear refresh token cookie."""
    Authorize.unset_jwt_cookies()
    return {"msg": "Successfully logged out"}
