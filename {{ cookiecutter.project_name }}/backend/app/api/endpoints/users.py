"""User endpoints."""
from typing import Any, List, Optional

from fastapi import APIRouter, Body, Depends, Header, HTTPException
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from app import crud, models, schemas
from app.api import deps

router = APIRouter()


@router.get("", response_model=List[schemas.User])
def list_users(
    *,
    db: Session = Depends(deps.get_db),
    skip: int = 0,
    limit: int = 100,
    current_user: models.User = Depends(deps.get_current_active_superuser),
) -> Any:
    """Retrieve users."""
    users = crud.user.get_multi(db, skip=skip, limit=limit)
    return users


@router.post("", response_model=schemas.User)
def create_user(
    *,
    db: Session = Depends(deps.get_db),
    password: str = Body(...),
    username: str = Body(...),
    full_name: str = Body(None),
    edipi: int = Body(None),
) -> Any:
    """Create new user."""
    user = crud.user.get_by_username(db, username=username)
    if user:
        raise HTTPException(
            status_code=400,
            detail="The user with this username already exists in the system",
        )
    user_in = schemas.UserCreate(
        password=password, username=username, full_name=full_name, edipi=edipi
    )
    user = crud.user.create(db, obj_in=user_in)
    return user


@router.patch("/me", response_model=schemas.User)
def update_self(
    *,
    db: Session = Depends(deps.get_db),
    user_in: schemas.UserUpdate,
    current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """Update own user."""
    return crud.user.update(db, db_obj=current_user, obj_in=user_in)


@router.get("/me", response_model=schemas.User)
def retrieve_self(
    db: Session = Depends(deps.get_db),
    current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """Get current user."""
    return current_user


@router.get("/{user_id}", response_model=schemas.User)
def retrieve_user(
    *,
    db: Session = Depends(deps.get_db),
    user_id: int,
    current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """Get a specific user by id."""
    user = crud.user.get(db, id=user_id)
    if user == current_user:
        return user
    if not crud.user.is_superuser(current_user):
        raise HTTPException(
            status_code=400, detail="The user doesn't have enough privileges"
        )
    return user


@router.patch("/{user_id}", response_model=schemas.User)
def update_user(
    *,
    db: Session = Depends(deps.get_db),
    user_id: int,
    user_in: schemas.UserUpdate,
    current_user: models.User = Depends(deps.get_current_active_superuser),
) -> Any:
    """Update a user."""
    user = crud.user.get(db, id=user_id)
    if not user:
        raise HTTPException(
            status_code=404,
            detail="The user with this id does not exist in the system",
        )
    user = crud.user.update(db, db_obj=user, obj_in=user_in)
    return user


@router.put("/link-cac", response_model=schemas.Msg)
def link_cac(
    *,
    db: Session = Depends(deps.get_db),
    current_user: models.User = Depends(deps.get_current_active_user),
    x_client_verified: Optional[str] = Header(None),
    x_subject_dn: Optional[str] = Header(None),
):
    """Link a CAC to the current user."""
    if x_client_verified == "NONE":
        return {"msg": "CAC not found"}
    try:
        edipi = x_subject_dn.split(",")[0].split(".")[-1]
    except KeyError:
        return {"msg": "Failed to extract EDIPI"}
    user = crud.user.get_by_edipi(db, edipi=edipi)
    if user is not None:
        return {"msg": "CAC already linked to another account"}

    current_user_data = jsonable_encoder(current_user)
    user_in = schemas.UserUpdate(**current_user_data)
    user_in.edipi = edipi

    crud.user.update(db, db_obj=current_user, obj_in=user_in)
    return {"msg": "CAC successfully linked to account"}
