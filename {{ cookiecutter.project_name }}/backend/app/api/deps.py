"""Dependencies for endpoint methods."""
from typing import Generator

from fastapi import Depends, HTTPException

# from fastapi.security import OAuth2PasswordBearer
from fastapi_jwt_auth import AuthJWT
from sqlalchemy.orm import Session

from app import crud, models
from app.db.session import SessionLocal

# reusable_oauth2 = OAuth2PasswordBearer(tokenUrl="/api/login/access-token")


def get_db() -> Generator:
    """Retrieve the database."""
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()


def get_current_user(
    db: Session = Depends(get_db),
    Authorize: AuthJWT = Depends(),
) -> models.User:
    """Retrieve the current user based on JWT credentials."""
    current_user_id = Authorize.get_jwt_subject()
    return crud.user.get(db, id=current_user_id)


def get_current_active_user(
    current_user: models.User = Depends(get_current_user),
) -> models.User:
    """Verify current user is active."""
    if not crud.user.is_active(current_user):
        raise HTTPException(status_code=400, detail="Inactive user")
    return current_user


def get_current_active_superuser(
    current_user: models.User = Depends(get_current_active_user),
) -> models.User:
    """Verify current user is superuser."""
    if not crud.user.is_superuser(current_user):
        raise HTTPException(
            status_code=400, detail="The user doesn't have enough privileges"
        )
    return current_user
