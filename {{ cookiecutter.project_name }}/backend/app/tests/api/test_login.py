"""api login tests."""
from typing import Dict

from fastapi.testclient import TestClient

from app.core.config import settings


def test_password_login(client: TestClient) -> None:
    """Test successful run of password_login function."""
    login_data = {
        "username": settings.FIRST_SUPERUSER,
        "password": settings.FIRST_SUPERUSER_PASSWORD,
    }
    r = client.post("/api/login/password", data=login_data)

    result_json = r.json()
    assert r.status_code == 200
    assert "accessToken" in result_json.keys()
    assert result_json["accessToken"] is not None
    result_headers = r.headers
    assert "set-cookie" in result_headers.keys()
    assert "refresh_token_cookie=" in result_headers["set-cookie"]


def test_use_access_token(
    client: TestClient, superuser_token_headers: Dict[str, str]
) -> None:
    """Test successful run of test_token function."""
    r = client.post(
        "/api/login/test-token",
        headers=superuser_token_headers,
    )

    result_json = r.json()
    assert r.status_code == 200
    assert "username" in result_json


def test_refresh_access_token(
    client: TestClient, superuser_token_cookies: Dict[str, str]
) -> None:
    """Test successful run of refresh_access_token function."""
    r = client.post("/api/login/refresh", cookies=superuser_token_cookies)

    result = r.json()
    assert r.status_code == 200
    assert "accessToken" in result.keys()
    assert result["accessToken"] is not None


def test_cac_login(client: TestClient, linked_cac_credentials: Dict[str, str]) -> None:
    """Test successful run of cac_login function."""
    r = client.post("/api/login/cac", headers=linked_cac_credentials)

    result_json = r.json()
    print(result_json)
    assert r.status_code == 200
    assert "accessToken" in result_json.keys()
    assert result_json["accessToken"] is not None
    result_headers = r.headers
    assert "set-cookie" in result_headers.keys()
    assert "refresh_token_cookie=" in result_headers["set-cookie"]


def test_logout(client: TestClient) -> None:
    """Test successful run of logout function."""
    r = client.get("/api/logout")

    result_json = r.json()
    assert "msg" in result_json.keys()
    assert result_json["msg"] == "Successfully logged out"
    result_headers = r.headers
    assert "set-cookie" in result_headers.keys()
    assert 'refresh_token_cookie=""' in result_headers["set-cookie"]
