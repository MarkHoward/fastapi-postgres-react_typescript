"""api users tests."""
from typing import Dict

from fastapi.testclient import TestClient
from sqlalchemy.orm import Session

from app import crud
from app.core.config import settings
from app.schemas.user import UserCreate
from app.tests.utils.utils import random_lower_string


def test_retrieve_self(
    client: TestClient, superuser_token_headers: Dict[str, str]
) -> None:
    """Test successful run of retrieve_self function."""
    r = client.get("/api/users/me", headers=superuser_token_headers)
    current_user = r.json()
    assert current_user
    assert current_user["isActive"]
    assert current_user["isSuperuser"]
    assert current_user["username"] == settings.FIRST_SUPERUSER


def test_create_user(
    client: TestClient, superuser_token_headers: dict, db: Session
) -> None:
    """Test successful run of create_user function."""
    username = random_lower_string()
    password = random_lower_string()
    data = {"username": username, "password": password}
    r = client.post(
        "/api/users",
        headers=superuser_token_headers,
        json=data,
    )
    assert r.status_code == 200
    created_user = r.json()
    user = crud.user.get_by_username(db, username=username)
    assert user is not None
    assert user.username == created_user["username"]


def test_create_user_existing_username(
    client: TestClient, superuser_token_headers: dict, db: Session
) -> None:
    """Test failed run of create_user function."""
    username = random_lower_string()
    password = random_lower_string()
    user_in = UserCreate(username=username, password=password)
    crud.user.create(db, obj_in=user_in)
    data = {"username": username, "password": password}
    r = client.post(
        "/api/users",
        headers=superuser_token_headers,
        json=data,
    )

    assert r.status_code == 400


def test_retrieve_user(
    client: TestClient, superuser_token_headers: dict, db: Session
) -> None:
    """Test successful run of retrieve_user function."""
    username = random_lower_string()
    password = random_lower_string()
    user_in = UserCreate(username=username, password=password)
    user = crud.user.create(db, obj_in=user_in)
    user_id = user.id
    r = client.get(
        f"/api/users/{user_id}",
        headers=superuser_token_headers,
    )
    assert 200 <= r.status_code < 300
    api_user = r.json()
    existing_user = crud.user.get_by_username(db, username=username)
    assert existing_user is not None
    assert existing_user.username == api_user["username"]


def test_list_users(
    client: TestClient, superuser_token_headers: dict, db: Session
) -> None:
    """Test successful run of list_users function."""
    username = random_lower_string()
    password = random_lower_string()
    user_in = UserCreate(username=username, password=password)
    crud.user.create(db, obj_in=user_in)

    username2 = random_lower_string()
    password2 = random_lower_string()
    user_in2 = UserCreate(username=username2, password=password2)
    crud.user.create(db, obj_in=user_in2)

    r = client.get("/api/users", headers=superuser_token_headers)
    all_users = r.json()

    assert len(all_users) > 1
    for item in all_users:
        assert "username" in item.keys()


def test_update_users(
    client: TestClient, superuser_token_headers: dict, db: Session
) -> None:
    """Test successful run of update_user function."""
    data = {"username": settings.FIRST_SUPERUSER, "edipi": 123456789}

    r = client.patch("/api/users/1", json=data, headers=superuser_token_headers)

    result = r.json()
    assert r.status_code == 200
    assert result["username"] == settings.FIRST_SUPERUSER
    assert result["edipi"] == 123456789


def test_link_cac(
    client: TestClient,
    superuser_token_headers: dict,
    db: Session,
    cac_credentials: dict,
) -> None:
    """Test successful run of link_cac function."""
    superuser_token_headers.update(cac_credentials)
    r = client.put("/api/users/link-cac", headers=superuser_token_headers)

    result = r.json()
    assert r.status_code == 200
    assert "msg" in result.keys()
    assert result["msg"] == "CAC successfully linked to account"
