"""Basic utilities."""
import random
import string
from typing import Dict

from fastapi.testclient import TestClient

from app.core.config import settings


def random_lower_string() -> str:
    """Return a string of random lower case characters."""
    return "".join(random.choices(string.ascii_lowercase, k=32))


def random_number(digits: int) -> int:
    """Return a random number with set number of digits."""
    return random.randrange(10 ** (digits - 1), 10 ** digits)


def get_superuser_token_headers(client: TestClient) -> Dict[str, str]:
    """Return headers containing authentication creds for the superuser."""
    login_data = {
        "username": settings.FIRST_SUPERUSER,
        "password": settings.FIRST_SUPERUSER_PASSWORD,
    }
    r = client.post("/api/login/password", data=login_data)

    result = r.json()
    a_token = result["accessToken"]
    headers = {"Authorization": f"Bearer {a_token}"}
    return headers


def get_superuser_token_cookies(client: TestClient) -> Dict[str, str]:
    """Return cookies containing refresh authentication creds for the superuser."""
    login_data = {
        "username": settings.FIRST_SUPERUSER,
        "password": settings.FIRST_SUPERUSER_PASSWORD,
    }
    r = client.post("/api/login/password", data=login_data)

    set_cookies = r.headers["set-cookie"]
    r_token = set_cookies.split("=")[1].split(";")[0]
    cookies = {"refresh_token_cookie": r_token}
    return cookies
