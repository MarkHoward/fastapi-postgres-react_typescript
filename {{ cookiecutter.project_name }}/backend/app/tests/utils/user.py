"""User utilities."""
from typing import Dict, Tuple

from fastapi.testclient import TestClient
from sqlalchemy.orm import Session

from app import crud
from app.models.user import User
from app.schemas.user import UserCreate
from app.tests.utils.utils import random_lower_string


def user_authentication_headers(
    client: TestClient, username: str, password: str
) -> Dict[str, str]:
    """Return headers containing authentication creds for a basic user."""
    data = {"username": username, "password": password}

    r = client.post("/api/login/password", data=data)
    response = r.json()
    auth_token = response["accessToken"]
    headers = {"Authorization": f"Bearer {auth_token}"}
    return headers


def create_random_user(db: Session) -> Tuple[User, str]:
    """Create a basic user with random username and password."""
    username = random_lower_string()
    password = random_lower_string()
    user_in = UserCreate(username=username, password=password)
    user = crud.user.create(db=db, obj_in=user_in)
    return user, password


def set_cac_authentication(
    client: TestClient,
    user: User,
    edipi: int,
    superuser_token_headers: Dict[str, str],
) -> None:
    """Links an EDIPI to a user for CAC authentication."""
    data = {"edipi": edipi, "username": user.username}
    client.patch(f"/api/users/{user.id}", json=data, headers=superuser_token_headers)
