"""Test setup."""
from typing import Dict, Generator

import pytest
from fastapi.testclient import TestClient

from app.db.session import SessionLocal
from app.main import app
from app.tests.utils.user import (
    create_random_user,
    user_authentication_headers,
    set_cac_authentication,
)
from app.tests.utils.utils import (
    get_superuser_token_cookies,
    get_superuser_token_headers,
)
from app.tests.mocks import get_mocked_cac_credentials


@pytest.fixture(scope="session")
def db() -> Generator:
    """Fixture to return a database object."""
    yield SessionLocal()


@pytest.fixture(scope="module")
def client() -> Generator:
    """Fixture to return a test client object."""
    with TestClient(app) as c:
        yield c


@pytest.fixture(scope="module")
def superuser_token_headers(client: TestClient) -> Dict[str, str]:
    """Fixture to return superuser credentials."""
    return get_superuser_token_headers(client)


@pytest.fixture(scope="module")
def superuser_token_cookies(client: TestClient) -> Dict[str, str]:
    """Fixture to return superuser refresh credentials."""
    return get_superuser_token_cookies(client)


@pytest.fixture(scope="module")
def normal_user_token_headers(client: TestClient) -> Dict[str, str]:
    """Fixture to return basic user credentials."""
    user, password = create_random_user(SessionLocal())
    return user_authentication_headers(client, user.username, password)


@pytest.fixture(scope="module")
def linked_cac_credentials(client: TestClient) -> Dict[str, str]:
    """Fixture to set and return CAC credentials."""
    user, password = create_random_user(SessionLocal())
    cac_credentials = get_mocked_cac_credentials()
    superuser_token_headers = get_superuser_token_headers(client)
    set_cac_authentication(
        client, user, cac_credentials["x-subject-dn"], superuser_token_headers
    )
    return cac_credentials


@pytest.fixture(scope="module")
def cac_credentials() -> Dict[str, str]:
    """Fixture to return unset CAC credentials."""
    return get_mocked_cac_credentials()
