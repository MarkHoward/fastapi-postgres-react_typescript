"""Mocked objects for tests."""
from typing import Dict
from app.tests.utils.utils import random_number


def get_mocked_cac_credentials() -> Dict[str, str]:
    """Mock successful CAC verification headers with random EDIPI."""
    edipi = str(random_number(10))
    return {"x-client-verified": "SUCCESS", "x-subject-dn": edipi}
