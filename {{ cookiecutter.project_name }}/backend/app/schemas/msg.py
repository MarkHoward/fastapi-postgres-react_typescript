"""Basic message schema."""
from fastapi_camelcase import CamelModel


class Msg(CamelModel):
    """Basic message schema."""
    msg: str
