"""User schemas."""
from typing import Optional

from fastapi_camelcase import CamelModel


class UserBase(CamelModel):
    """Shared properties."""
    username: str
    is_active: Optional[bool] = True
    is_superuser: Optional[bool] = False
    full_name: Optional[str] = None
    edipi: Optional[int] = None


class UserCreate(UserBase):
    """Properties to receive via API on creation."""
    password: str


class UserUpdate(UserBase):
    """Properties to receive via API on update."""
    password: Optional[str] = None


class UserInDBBase(UserBase):
    """Shared properties if exists in DB."""
    id: Optional[int] = None

    class Config:
        """Configuration."""
        orm_mode = True


class User(UserInDBBase):
    """Additional properties to return via API."""
    pass


class UserInDB(UserInDBBase):
    """Additional properties stored in DB."""
    hashed_password: str
