"""Schemas module."""
# flake8: noqa
from app.schemas.msg import Msg
from app.schemas.token import Token, TokenPayload
from app.schemas.user import User, UserCreate, UserInDB, UserUpdate
