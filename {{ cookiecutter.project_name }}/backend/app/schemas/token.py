"""Token schemas."""
from typing import Optional

from fastapi_camelcase import CamelModel


class Token(CamelModel):
    """Token schema."""
    access_token: str
    token_type: str


class TokenPayload(CamelModel):
    """Token payload schema."""
    sub: Optional[int] = None
