import { createContext, Dispatch, SetStateAction } from "react";
import { User } from "./types";

interface SessionContext {
  user: User;
  setUser: Dispatch<SetStateAction<User>>;
  syncLogout: Function;
  refresh: Function;
}

const dispatch: Dispatch<SetStateAction<User>> = () => {};

const sessionContext: SessionContext = {
  user: {
    id: 0,
    isSuperUser: false,
  },
  setUser: dispatch,
  syncLogout: () => {},
  refresh: () => {},
};

export default createContext(sessionContext);
