export type User = {
    id: number
    isSuperUser: boolean,
}

export type decodedToken = {
    sub: number,
    isSuperUser: boolean,
}
