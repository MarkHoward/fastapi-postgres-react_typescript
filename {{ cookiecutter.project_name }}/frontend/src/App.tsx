import React, { useCallback, useEffect, useState } from "react";
import { ThemeProvider } from "@material-ui/core/styles";
import Theme from "Theme";
import Header from "components/Header";
import Sidebar from "components/Sidebar";
import Main from "components/Main";
import SessionContext from "SessionContext";
import { CssBaseline, makeStyles } from "@material-ui/core";
import { BrowserRouter as Router } from "react-router-dom";
import axios from "axios";
import jwt_decode from "jwt-decode";
import AlertBar, { AlertBarContext } from "components/AlertBar";
import { Color } from "@material-ui/lab/Alert";
import { decodedToken, User } from "types";

const useStyles = makeStyles(() => ({
  app: {
    display: "flex",
  },
}));

const App: React.FC = () => {
  const classes = useStyles();

  const [user, setUser] = useState<User>({
    id: 0,
    isSuperUser: false,
  });

  const [open, setOpen] = useState<boolean>(false);
  const [alertText, setAlertText] = useState<string>("");
  const [severity, setSeverity] = useState<Color>("error");

  const syncLogout = useCallback(() => {
    delete axios.defaults.headers.common["Authorization"];
    setUser({
      id: 0,
      isSuperUser: false,
    });
  }, []);

  const login = useCallback((accessToken) => {
    axios.defaults.headers.common["Authorization"] = `Bearer ${accessToken}`;
    const decoded: decodedToken = jwt_decode(accessToken);
    setUser({ id: decoded.sub, isSuperUser: decoded.isSuperUser });
  }, []);

  const refresh = useCallback(async () => {
    try {
      const response = await axios.post("/api/login/refresh");
      login(response.data.accessToken);
      // token is good for 900 seconds - refresh every 870 seconds, or 870000 ms
      setTimeout(refresh, 870000);
    } catch (error) {
      syncLogout();
    }
  }, [login, syncLogout]);

  const cacLogin = useCallback(async () => {
    const response = await axios.post("/api/login/cac");
    login(response.data.accessToken);
    // token is good for 900 seconds - refresh every 870 seconds, or 870000 ms
    setTimeout(refresh, 870000);
  }, [login, refresh]);

  useEffect(() => {
    cacLogin();
  }, [cacLogin]);

  window.onstorage = () => {
    refresh();
  };

  return (
    <SessionContext.Provider value={{ user, setUser, refresh, syncLogout }}>
      <AlertBarContext.Provider value={{ setOpen, setAlertText, setSeverity }}>
        <ThemeProvider theme={Theme}>
          <div className={classes.app}>
            <Router>
              <CssBaseline />
              <Header />
              <Sidebar />
              <Main />
              <AlertBar
                open={open}
                onClose={() => setOpen(false)}
                alertText={alertText}
                severity={severity}
              />
            </Router>
          </div>
        </ThemeProvider>
      </AlertBarContext.Provider>
    </SessionContext.Provider>
  );
};

export default App;
