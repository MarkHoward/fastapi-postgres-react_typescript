const resetMocks = () => {
  axios.get.mockClear();
  axios.post.mockClear();
  axios.patch.mockClear();
  axios.put.mockClear();
  axios.delete.mockClear();
};

const axios = {
  get: jest.fn(),
  post: jest.fn(),
  patch: jest.fn(),
  put: jest.fn(),
  delete: jest.fn(),
  reset: resetMocks,
};

export default axios;
