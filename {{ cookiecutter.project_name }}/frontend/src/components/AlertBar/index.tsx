import { Snackbar } from "@material-ui/core";
import MuiAlert, { Color } from "@material-ui/lab/Alert";
import { context } from "./context";
import React from "react";

interface AlertBarProps {
  open: boolean;
  onClose: () => void;
  alertText: string;
  severity: Color;
}

const autoHideMilliSecs = 6000;
const alertElevationLevel = 6;

const AlertBar = (props: AlertBarProps) => {
  const { open, onClose, alertText, severity } = props;
  return (
    <>
      <Snackbar
        open={open}
        onClose={onClose}
        autoHideDuration={autoHideMilliSecs}
      >
        <MuiAlert
          variant="filled"
          elevation={alertElevationLevel}
          severity={severity}
        >
          {alertText}
        </MuiAlert>
      </Snackbar>
    </>
  );
};

export const AlertBarContext = context;
export default AlertBar;
