import React from "react";
import { render } from "@testing-library/react";
import AlertBar from ".";

describe("<AlertBar />", () => {
  it("displays with given text", () => {
    const { queryByText } = render(
      <AlertBar
        open
        onClose={() => {}}
        alertText="Mock Text"
        severity="success"
      />)
      expect(queryByText("Mock Text")).toBeInTheDocument
  });
});
