import { Color } from "@material-ui/lab/Alert";
import { createContext, Dispatch, SetStateAction } from "react";

interface AlertBarContext {
  setOpen: Dispatch<SetStateAction<boolean>>;
  setAlertText: Dispatch<SetStateAction<string>>;
  setSeverity: Dispatch<SetStateAction<Color>>;
}

const dispatchOpen: Dispatch<SetStateAction<boolean>> = () => {};
const dispatchAlertText: Dispatch<SetStateAction<string>> = () => {};
const dispatchSeverity: Dispatch<SetStateAction<Color>> = () => {};

const alertBarContext: AlertBarContext = {
  setOpen: dispatchOpen,
  setAlertText: dispatchAlertText,
  setSeverity: dispatchSeverity,
};

export const context = createContext(alertBarContext);
