import React from "react";
import { render } from "@testing-library/react";
import Header from "."

describe("<Header />", () => {
    it("should render with text for of the project name", () => {
        const { getByText } = render(<Header />)
        expect(getByText("project name")).toBeInTheDocument;
    })
})
