import {
  Button,
  Checkbox,
  CircularProgress,
  FormControlLabel,
  FormControlLabelProps,
  Grid,
  TextField,
  TextFieldProps,
} from "@material-ui/core";
import axios from "axios";
import React, {
  ChangeEventHandler,
  useCallback,
  useContext,
  useEffect,
  useState,
} from "react";
import { useHistory, useParams } from "react-router-dom";
import { AlertBarContext } from "components/AlertBar";
import useFormReducer, {
  setUsername,
  setPassword,
  setEdipi,
  setActive,
  setSuperuser,
} from "./reducer";

interface RouteParams {
  user: string;
}

const UpdateUser: React.FC = () => {
  let params = useParams<RouteParams>();
  let history = useHistory();

  const [formState, dispatch] = useFormReducer();
  const { username, password, edipi, isActive, isSuperuser } = formState;
  const [loading, setLoading] = useState(false);

  const { setOpen, setAlertText, setSeverity } = useContext(AlertBarContext);

  const fetchData = useCallback(async () => {
    const response = await axios.get(`/api/users/${params.user}`);
    dispatch(setUsername(response.data.username));
    dispatch(setEdipi(response.data.edipi));
    dispatch(setActive(response.data.isActive));
    dispatch(setSuperuser(response.data.isSuperuser));
  }, [params.user, dispatch]);

  useEffect(() => {
    fetchData();
  }, [fetchData]);

  const handleChange: ChangeEventHandler<HTMLInputElement> = (e) => {
    switch (e.target.name) {
      case "password":
        dispatch(setPassword(e.target.value));
        break;
      case "edipi":
        dispatch(setEdipi(e.target.value));
        break;
      case "isActive":
        dispatch(setActive(e.target.checked));
        break;
      case "isSuperuser":
        dispatch(setSuperuser(e.target.checked));
        break;
    }
  };

  const handleSubmit = async () => {
    dispatch(setPassword(password.value));
    dispatch(setEdipi(edipi.value));
    if (password.error || edipi.error) {
      return;
    }
    setLoading(true);
    try {
      await axios.patch(`/api/users/${params.user}`, {
        username: username.value,
        password: password.value,
        edipi: Number(edipi.value) || null,
        isActive: isActive.value,
        isSuperuser: isSuperuser.value,
      });
      setAlertText("Successfully updated user");
      setSeverity("success");
      history.push("/admin");
    } catch (error) {
      setAlertText(error.response.data.detail);
      setSeverity("error");
    }
    setOpen(true);
    setLoading(false);
  };

  const usernameFieldProps: TextFieldProps = {
    label: "Username",
    name: "username",
    disabled: true,
    ...username,
  };

  const passwordFieldProps: TextFieldProps = {
    label: "Password",
    name: "password",
    type: "password",
    onChange: handleChange,
    ...password,
  };

  const edipiFieldProps: TextFieldProps = {
    label: "EDIPI",
    name: "edipi",
    type: "number",
    InputLabelProps: { shrink: !!edipi.value },
    onChange: handleChange,
    ...edipi,
  };

  const activeCheckboxLabelProps: FormControlLabelProps = {
    control: (
      <Checkbox
        name="isActive"
        checked={isActive.value}
        onChange={handleChange}
      />
    ),
    label: "Active",
  };

  const adminCheckboxLabelProps: FormControlLabelProps = {
    control: (
      <Checkbox
        name="isSuperuser"
        checked={isSuperuser.value}
        onChange={handleChange}
      />
    ),
    label: "Admin",
  };

  return (
    <>
      <Grid item>
        <TextField {...usernameFieldProps} />
      </Grid>
      <Grid item>
        <TextField {...passwordFieldProps} />
      </Grid>
      <Grid item>
        <TextField {...edipiFieldProps} />
      </Grid>
      <Grid item>
        <FormControlLabel {...activeCheckboxLabelProps} />
      </Grid>
      <Grid item>
        <FormControlLabel {...adminCheckboxLabelProps} />
      </Grid>
      <Grid item>
        <Button variant="contained" onClick={handleSubmit} disabled={loading}>
          Submit
          {loading && <CircularProgress size={25} />}
        </Button>
      </Grid>
    </>
  );
};

export default UpdateUser;
