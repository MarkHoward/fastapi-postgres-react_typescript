import passwordValidator from "helpers/passwordValidator";
import { useReducer } from "react";

export const setUsername = (username: string) => {
  return <const>{
    type: "SET_USERNAME",
    value: username,
  };
};

export const setPassword = (password: string) => {
  return <const>{
    type: "SET_PASSWORD",
    value: password,
  };
};

export const setEdipi = (edipi: string) => {
  return <const>{
    type: "SET_EDIPI",
    value: edipi,
  };
};

export const setActive = (active: boolean) => {
  return <const>{
    type: "SET_ACTIVE",
    value: active,
  };
};

export const setSuperuser = (superuser: boolean) => {
  return <const>{
    type: "SET_SUPERUSER",
    value: superuser,
  };
};

const initialState = {
  username: { value: "" },
  password: { value: "", error: false, helperText: "" },
  edipi: { value: "", error: false, helperText: "" },
  isActive: { value: false },
  isSuperuser: { value: false },
};

type State = typeof initialState;

type Action = ReturnType<
  | typeof setUsername
  | typeof setPassword
  | typeof setEdipi
  | typeof setActive
  | typeof setSuperuser
>;

const reducer = (state: State, action: Action): State => {
  let error: boolean, helperText: string;
  switch (action.type) {
    case "SET_USERNAME":
      return {
        ...state,
        username: { value: action.value },
      };
    case "SET_PASSWORD":
      ({ error, helperText } = validatePassword(action.value));
      return {
        ...state,
        password: { value: action.value, error, helperText },
      };
    case "SET_EDIPI":
      ({ error, helperText } = validateEdipi(action.value));
      return {
        ...state,
        edipi: { value: action.value, error, helperText },
      };
    case "SET_ACTIVE":
      return {
        ...state,
        isActive: { value: action.value },
      };
    case "SET_SUPERUSER":
      return {
        ...state,
        isSuperuser: { value: action.value },
      };
  }
};

const validatePassword = (password: string) => {
  if (password === "") {
    return { error: false, helperText: "" };
  }
  const validationErrors = passwordValidator(password);
  return {
    error: !!validationErrors,
    helperText: validationErrors,
  };
};

const validateEdipi = (edipi: string) => {
  if (edipi) {
    let num = parseInt(edipi);
    if (isNaN(num)) {
      return { error: true, helperText: "Not a number" };
    }
  }
  return { error: false, helperText: "" };
};

export default function useLoginFormReducer(state = initialState) {
  return useReducer(reducer, state);
}
