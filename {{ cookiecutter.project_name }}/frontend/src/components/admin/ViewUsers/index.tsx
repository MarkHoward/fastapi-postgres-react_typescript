import {
  Checkbox,
  makeStyles,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TableSortLabel,
} from "@material-ui/core";
import axios from "axios";
import React, { useCallback, useEffect } from "react";
import { useHistory } from "react-router-dom";
import useSortReducer, { setUsers, adjustOrdering } from "./reducer";

const useStyles = makeStyles(() => ({
  table: {
    width: "80%",
  },
  row: {
    cursor: "pointer",
  },
}));

const ViewUsers: React.FC = () => {
  const classes = useStyles();
  let history = useHistory();

  const [sortState, dispatch] = useSortReducer();
  const { users, orderBy, direction } = sortState;

  const fetchData = useCallback(async () => {
    const response = await axios.get("/api/users");
    dispatch(setUsers(response.data));
  }, [dispatch]);

  useEffect(() => {
    fetchData();
  }, [fetchData]);

  const handleHeaderClick = (header: string) => {
    dispatch(adjustOrdering(header));
  };

  const handleUserSelect = (id: number) => {
    history.push(`/admin/users/${id}`);
  };

  return (
    <>
      <Table stickyHeader className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell sortDirection={orderBy === "id" ? direction : false}>
              <TableSortLabel
                active={orderBy === "id"}
                direction={direction}
                onClick={() => handleHeaderClick("id")}
              >
                ID
              </TableSortLabel>
            </TableCell>
            <TableCell
              sortDirection={orderBy === "username" ? direction : false}
            >
              <TableSortLabel
                active={orderBy === "username"}
                direction={direction}
                onClick={() => handleHeaderClick("username")}
              >
                Username
              </TableSortLabel>
            </TableCell>
            <TableCell
              padding="checkbox"
              sortDirection={orderBy === "isActive" ? direction : false}
            >
              <TableSortLabel
                active={orderBy === "isActive"}
                direction={direction}
                onClick={() => handleHeaderClick("isActive")}
              >
                Active
              </TableSortLabel>
            </TableCell>
            <TableCell
              padding="checkbox"
              sortDirection={orderBy === "isSuperuser" ? direction : false}
            >
              <TableSortLabel
                active={orderBy === "isSuperuser"}
                direction={direction}
                onClick={() => handleHeaderClick("isSuperuser")}
              >
                Admin
              </TableSortLabel>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {users.map((user) => (
            <TableRow
              key={user.id}
              className={classes.row}
              onClick={() => handleUserSelect(user.id)}
            >
              <TableCell>{user.id}</TableCell>
              <TableCell>{user.username}</TableCell>
              <TableCell padding="checkbox">
                <Checkbox checked={user.isActive} disabled={true} />
              </TableCell>
              <TableCell padding="checkbox">
                <Checkbox checked={user.isSuperuser} disabled={true} />
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </>
  );
};

export default ViewUsers;
