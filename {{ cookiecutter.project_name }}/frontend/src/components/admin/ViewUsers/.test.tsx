import React from "react";
import { render, waitFor } from "@testing-library/react";
import ViewUsers from ".";
import axios from "__mocks__/axios";

const user1 = { id: 1, username: "d-user", isActive: true, isSuperuser: true };
const user2 = { id: 2, username: "c-user", isActive: true, isSuperuser: false };
const user3 = {
  id: 3,
  username: "b-user",
  isActive: false,
  isSuperuser: false,
};
const user4 = { id: 4, username: "a-user", isActive: true, isSuperuser: true };

const responseData = { data: [user2, user1, user4, user3] };

const sortedByIdAsc = [user1, user2, user3, user4];
const sortedByUsernameAsc = [user4, user3, user2, user1];
const sortedByIsActiveAsc = [user1, user2, user4, user3];
const sortedByIsSuperuserAsc = [user1, user4, user2, user3];

describe("<ViewUsers />", () => {
  beforeEach(() => {
    axios.get.mockResolvedValue({ responseData });
  });

  afterEach(() => {
    axios.reset();
  });

  it("should default to display data by ID ascending", async () => {
    const { queryAllByText } = render(<ViewUsers />);
    await waitFor(() => {
      const users = queryAllByText("/[a-d]-user/");
      for (var i = 0; i < users.length; i++) {
        expect(users[i]).toHaveTextContent(sortedByIdAsc[i].username);
      }
    });
  });
});
