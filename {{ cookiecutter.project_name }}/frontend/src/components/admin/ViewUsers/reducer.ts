import { useReducer } from "react";

type Order = "asc" | "desc";
type User = {
  id: number;
  username: string;
  isActive: boolean;
  isSuperuser: boolean;
};

export const setUsers = (users: Array<User>) => {
  return <const>{
    type: "SET_USERS",
    value: users,
  };
};

export const adjustOrdering = (header: string) => {
  return <const>{
    type: "ADJUST_ORDERING",
    value: header,
  };
};

const initialState = {
  users: [],
  direction: "asc" as Order,
  orderBy: "id",
};

type State = typeof initialState;

type Action = ReturnType<typeof setUsers | typeof adjustOrdering>;

const reducer = (state: State, action: Action): State => {
  let users: Array<User>;
  switch (action.type) {
    case "SET_USERS":
      users = sort(action.value, state.orderBy, state.direction);
      return { ...state, users };
    case "ADJUST_ORDERING":
      if (action.value === state.orderBy) {
        let direction =
          state.direction === "asc" ? ("desc" as Order) : ("asc" as Order);
        users = sort(state.users, state.orderBy, direction);
        return { ...state, users, direction };
      }
      let direction = "asc" as Order;
      users = sort(state.users, action.value, direction);
      return { users, direction, orderBy: action.value };
  }
};

const sort = (users: Array<User>, orderBy: string, direction: Order) => {
  return users.sort((a, b) => {
    let comparison = 0;
    if (a[orderBy] < b[orderBy]) {
      comparison = -1;
    } else if (a[orderBy] > b[orderBy]) {
      comparison = 1;
    }
    if (direction === "desc") {
      comparison *= -1;
    }
    return comparison;
  });
};

export default function useViewUsersSortReducer(state = initialState) {
  return useReducer(reducer, state);
}
