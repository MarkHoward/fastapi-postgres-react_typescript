import axios from "axios";
import React, { useCallback, useContext, useEffect, useState } from "react";
import SessionContext from "SessionContext";
import {
  Backdrop,
  CircularProgress,
  Grid,
  makeStyles,
  Typography,
} from "@material-ui/core";

const sidebarWidth = 200;

const useStyles = makeStyles(() => ({
  backdrop: {
    left: sidebarWidth,
  },
}));

const Logout: React.FC = () => {
  const [loading, setLoading] = useState(true);
  const { syncLogout } = useContext(SessionContext);
  const classes = useStyles();

  const logout = useCallback(async () => {
    await axios.get("/api/logout");
    setLoading(false);
    localStorage.setItem("logout", Date.now().toString());
    syncLogout();
  }, [syncLogout]);

  useEffect(() => {
    logout();
  }, [logout]);

  return (
    <>
      <Backdrop className={classes.backdrop} open={loading}>
        <CircularProgress />
      </Backdrop>
      <Grid item>
        <Typography>
          {loading ? "Logging out..." : "Successfully logged out!"}
        </Typography>
      </Grid>
    </>
  );
};

export default Logout;
