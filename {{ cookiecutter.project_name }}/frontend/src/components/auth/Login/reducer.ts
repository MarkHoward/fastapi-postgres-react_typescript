import { useReducer } from "react";

export const setUsername = (username: string) => {
  return <const>{
    type: "SET_USERNAME",
    value: username,
  };
};

export const setPassword = (password: string) => {
  return <const>{
    type: "SET_PASSWORD",
    value: password,
  };
};

const initialState = {
  username: { value: "", error: false, helperText: "" },
  password: { value: "", error: false, helperText: "" },
};

type State = typeof initialState;

type Action = ReturnType<typeof setUsername | typeof setPassword>;

const reducer = (state: State, action: Action): State => {
  let error: boolean, helperText: string;
  switch (action.type) {
    case "SET_USERNAME":
      ({ error, helperText } = validateUsername(action.value));
      return {
        ...state,
        username: { value: action.value, error, helperText },
      };
    case "SET_PASSWORD":
      ({ error, helperText } = validatePassword(action.value));
      return {
        ...state,
        password: { value: action.value, error, helperText },
      };
  }
};

const validateUsername = (username: string) => {
  if (username === "") {
    return { error: true, helperText: "Please enter a username" };
  }
  return { error: false, helperText: "" };
};

const validatePassword = (password: string) => {
  if (password === "") {
    return { error: true, helperText: "Please enter a password" };
  }
  return { error: false, helperText: "" };
};

export default function useLoginFormReducer(state = initialState) {
  return useReducer(reducer, state);
}
