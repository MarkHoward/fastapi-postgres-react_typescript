import {
  Button,
  CircularProgress,
  Grid,
  TextField,
  TextFieldProps,
} from "@material-ui/core";
import axios from "axios";
import React, { ChangeEventHandler, useContext, useState } from "react";
import SessionContext from "SessionContext";
import { decodedToken } from "types";
import jwt_decode from "jwt-decode";
import { AlertBarContext } from "components/AlertBar";
import useFormReducer, {
  setUsername,
  setPassword1,
  setPassword2,
} from "./reducer";
import { useHistory } from "react-router-dom";

const Register: React.FC = () => {
  const { setUser, refresh } = useContext(SessionContext);
  const history = useHistory();

  const [formState, dispatch] = useFormReducer();
  const { username, password1, password2 } = formState;
  const [loading, setLoading] = useState(false);

  const { setOpen, setAlertText, setSeverity } = useContext(AlertBarContext);

  const handleChange: ChangeEventHandler<HTMLInputElement> = (e) => {
    switch (e.target.name) {
      case "username":
        dispatch(setUsername(e.target.value));
        break;
      case "password1":
        dispatch(setPassword1(e.target.value));
        break;
      case "password2":
        dispatch(setPassword2(e.target.value));
        break;
    }
  };

  const handleSubmit = async () => {
    dispatch(setUsername(username.value));
    dispatch(setPassword1(password1.value));
    dispatch(setPassword2(password2.value));
    if (username.error || password1.error || password2.error) {
      return;
    }
    setLoading(true);
    try {
      await axios.post("/api/users", {
        username: username.value,
        password: password1.value,
      });
      var formData = new FormData();
      formData.append("username", username.value);
      formData.append("password", password1.value);

      const response = await axios.post("/api/login/password", formData);
      axios.defaults.headers.common[
        "Authorization"
      ] = `Bearer ${response.data.accessToken}`;

      history.push("/");

      const decoded: decodedToken = jwt_decode(response.data.accessToken);
      setUser({ id: decoded.sub, isSuperUser: decoded.isSuperUser });
      // token is good for 900 seconds - refresh every 870 seconds, or 870000 ms
      setTimeout(refresh, 870000);
    } catch (error) {
      setAlertText(error.response.data.detail);
      setSeverity("error");
      setOpen(true);
    }
    setLoading(false);
  };

  const usernameFieldProps: TextFieldProps = {
    label: "Username",
    name: "username",
    onChange: handleChange,
    ...username,
  };

  const password1FieldProps: TextFieldProps = {
    label: "Password",
    name: "password1",
    type: "password",
    onChange: handleChange,
    ...password1,
  };

  const password2FieldProps: TextFieldProps = {
    label: "Verify Password",
    name: "password2",
    type: "password",
    onChange: handleChange,
    ...password2,
  };

  return (
    <>
      <Grid item>
        <TextField {...usernameFieldProps} />
      </Grid>
      <Grid item>
        <TextField {...password1FieldProps} />
      </Grid>
      <Grid item>
        <TextField {...password2FieldProps} />
      </Grid>
      <Grid item>
        <Button variant="contained" onClick={handleSubmit} disabled={loading}>
          Submit
          {loading && <CircularProgress size={25} />}
        </Button>
      </Grid>
    </>
  );
};

export default Register;
