import passwordValidator from "helpers/passwordValidator";
import { useReducer } from "react";

export const setUsername = (username: string) => {
  return <const>{
    type: "SET_USERNAME",
    value: username,
  };
};

export const setPassword1 = (password: string) => {
  return <const>{
    type: "SET_PASSWORD1",
    value: password,
  };
};

export const setPassword2 = (password: string) => {
  return <const>{
    type: "SET_PASSWORD2",
    value: password,
  };
};

const initialState = {
  username: { value: "", error: false, helperText: "" },
  password1: { value: "", error: false, helperText: "" },
  password2: { value: "", error: false, helperText: "" },
};

type State = typeof initialState;

type Action = ReturnType<
  typeof setUsername | typeof setPassword1 | typeof setPassword2
>;

const reducer = (state: State, action: Action): State => {
  let error: boolean, helperText: string;
  switch (action.type) {
    case "SET_USERNAME":
      ({ error, helperText } = validateUsername(action.value));
      return {
        ...state,
        username: { value: action.value, error, helperText },
      };
    case "SET_PASSWORD1":
      ({ error, helperText } = validatePassword(
        action.value,
        state.password2.value
      ));
      return {
        ...state,
        password1: { value: action.value, error, helperText },
      };
    case "SET_PASSWORD2":
      ({ error, helperText } = validatePassword(
        action.value,
        state.password1.value
      ));
      return {
        ...state,
        password2: { value: action.value, error, helperText },
      };
  }
};

const validateUsername = (username: string) => {
  if (username === "") {
    return { error: true, helperText: "Please enter a username" };
  }
  return { error: false, helperText: "" };
};

const validatePassword = (password: string, otherPassword: string) => {
  if (password === "") {
    return { error: true, helperText: "Please enter a password" };
  }
  if (otherPassword !== "" && password !== otherPassword) {
    return { error: true, helperText: "Passwords do not match" };
  }

  const validationErrors = passwordValidator(password);

  return {
    error: !!validationErrors,
    helperText: validationErrors,
  };
};

export default function useRegisterFormReducer(state = initialState) {
  return useReducer(reducer, state);
}
