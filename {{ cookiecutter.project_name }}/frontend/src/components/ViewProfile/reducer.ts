import { useReducer } from "react";
import passwordValidator from "helpers/passwordValidator";

export const setUsername = (username: string) => {
  return <const>{
    type: "SET_USERNAME",
    value: username,
  };
};

export const setFullName = (fullName: string) => {
  return <const>{
    type: "SET_FULL_NAME",
    value: fullName,
  };
};

export const setPassword1 = (password: string) => {
  return <const>{
    type: "SET_PASSWORD1",
    value: password,
  };
};

export const setPassword2 = (password: string) => {
  return <const>{
    type: "SET_PASSWORD2",
    value: password,
  };
};

const initialState = {
  username: { value: "" },
  fullName: { value: "", error: false, helperText: "" },
  password1: { value: "", error: false, helperText: "" },
  password2: { value: "", error: false, helperText: "" },
};

type State = typeof initialState;

type Action = ReturnType<
  | typeof setUsername
  | typeof setFullName
  | typeof setPassword1
  | typeof setPassword2
>;

const reducer = (state: State, action: Action): State => {
  let error: boolean, helperText: string;
  switch (action.type) {
    case "SET_USERNAME":
      return {
        ...state,
        username: { value: action.value },
      };
    case "SET_FULL_NAME":
      ({ error, helperText } = validateFullName(action.value));
      return {
        ...state,
        fullName: { value: action.value, error, helperText },
      };
    case "SET_PASSWORD1":
      ({ error, helperText } = validatePassword(
        action.value,
        state.password2.value
      ));
      return {
        ...state,
        password1: { value: action.value, error, helperText },
      };
    case "SET_PASSWORD2":
      ({ error, helperText } = validatePassword(
        action.value,
        state.password1.value
      ));
      return {
        ...state,
        password2: { value: action.value, error, helperText },
      };
  }
};

const validateFullName = (fullName: string) => {
  return { error: false, helperText: "" };
};

const validatePassword = (password: string, otherPassword: string) => {
  if (password === "") {
    return { error: true, helperText: "Please enter a password" };
  }
  if (otherPassword !== "" && password !== otherPassword) {
    return { error: true, helperText: "Passwords do not match" };
  }

  const validationErrors = passwordValidator(password);

  return {
    error: !!validationErrors,
    helperText: validationErrors,
  };
};

export default function useProfileFormReducer(state = initialState) {
  return useReducer(reducer, state);
}
