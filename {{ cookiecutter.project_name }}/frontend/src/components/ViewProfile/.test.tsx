import React from "react";
import { fireEvent, render, waitFor } from "@testing-library/react";
import ViewProfile from ".";
import axios from "__mocks__/axios";

describe("<ViewProfile />", () => {
  afterEach(() => {
    axios.reset();
  });

  it("should call /api/users/me on load", async () => {
    axios.get.mockResolvedValue({
      data: {
        username: "mock-username",
        fullName: "mock-fullName",
      },
    });
    const { queryByDisplayValue } = render(<ViewProfile />);
    expect(axios.get).toBeCalledWith("/api/users/me");
    await waitFor(
      () => expect(queryByDisplayValue("mock-username")).toBeInTheDocument
    );
    await waitFor(
      () => expect(queryByDisplayValue("mock-fullName")).toBeInTheDocument
    );
  });

  describe("Form structure", () => {
    it("should have text fields to change password", () => {
      const { queryByLabelText } = render(<ViewProfile />);
      expect(queryByLabelText("New Password")).toBeInTheDocument;
      expect(queryByLabelText("Confirm Password")).toBeInTheDocument;
    });
    it("should have a button to submit password change", () => {
      const { queryByText } = render(<ViewProfile />);
      expect(queryByText("Change Password")).toBeInTheDocument;
    });

    it("should have a username text field", () => {
      const { queryByLabelText } = render(<ViewProfile />);
      expect(queryByLabelText("Username")).toBeInTheDocument;
    });

    it("should have a full name text field", () => {
      const { queryByLabelText } = render(<ViewProfile />);
      expect(queryByLabelText("Full Name")).toBeInTheDocument;
    });
    it("should have a button to submit profile change", () => {
      const { queryByText } = render(<ViewProfile />);
      expect(queryByText("Update Profile")).toBeInTheDocument;
    });

    it("should have a button to link CAC", () => {
      const { queryByText } = render(<ViewProfile />);
      expect(queryByText("Link CAC")).toBeInTheDocument;
    });
  });

  describe("Form submit", () => {
    it("should submit password changes", () => {
      const { queryByLabelText, queryByText } = render(<ViewProfile />);
      fireEvent.change(queryByLabelText("New Password"), {
        target: { value: "P@ssword1" },
      });
      fireEvent.change(queryByLabelText("Confirm Password"), {
        target: { value: "P@ssword1" },
      });
      fireEvent.click(queryByText("Change Password"));
      expect(axios.patch).toBeCalledWith("/api/users/me", {
        username: "",
        password: "P@ssword1",
      });
    });
    it("should validate password input", () => {
      const { queryByLabelText, queryByText, queryAllByText } = render(
        <ViewProfile />
      );
      fireEvent.change(queryByLabelText("New Password"), {
        target: { value: "P@ssword1" },
      });
      fireEvent.change(queryByLabelText("Confirm Password"), {
        target: { value: "P@ssword2" },
      });
      fireEvent.click(queryByText("Change Password"));
      expect(axios.patch).toBeCalledTimes(0);
      expect(queryAllByText("Passwords do not match")).toBeInTheDocument;
    });

    it("should submit profile changes", () => {
      const { queryByLabelText, queryByText } = render(<ViewProfile />);
      fireEvent.change(queryByLabelText("Full Name"), {
        target: { value: "John Doe" },
      });
      fireEvent.click(queryByText("Update Profile"));
      expect(axios.patch).toBeCalledWith("/api/users/me", {
        username: "",
        fullName: "John Doe",
      });
    });

    it("should submit CAC link rquests", () => {
      const { queryByText } = render(<ViewProfile />);
      fireEvent.click(queryByText("Link CAC"));
      expect(axios.put).toBeCalledWith("/api/users/link-cac");
    });
  });
});
