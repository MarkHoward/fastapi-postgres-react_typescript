import { Button, CircularProgress, Grid, TextField, TextFieldProps } from "@material-ui/core";
import axios from "axios";
import React, {
  ChangeEventHandler,
  useCallback,
  useContext,
  useEffect,
  useState,
} from "react";
import { AlertBarContext } from "components/AlertBar";
import useFormReducer, {
  setUsername,
  setFullName,
  setPassword1,
  setPassword2,
} from "./reducer";

const ViewProfile: React.FC = () => {
  const { setOpen, setAlertText, setSeverity } = useContext(AlertBarContext);

  const [formState, dispatch] = useFormReducer();
  const { username, fullName, password1, password2 } = formState;
  const [loading, setLoading] = useState(false);

  const fetchData = useCallback(async () => {
    const response = await axios.get("/api/users/me");
    dispatch(setUsername(response.data.username));
    dispatch(setFullName(response.data.fullName));
  }, [dispatch]);

  useEffect(() => {
    fetchData();
  }, [fetchData]);

  const handleChange: ChangeEventHandler<HTMLInputElement> = (e) => {
    switch (e.target.name) {
      case "fullName":
        dispatch(setFullName(e.target.value));
        break;
      case "password1":
        dispatch(setPassword1(e.target.value));
        break;
      case "password2":
        dispatch(setPassword2(e.target.value));
        break;
    }
  };

  const handleProfileUpdate = async () => {
    dispatch(setFullName(fullName.value));
    if (fullName.error) {
      return;
    }
    setLoading(true);
    try {
      await axios.patch("/api/users/me", {
        username: username.value,
        fullName: fullName.value,
      });
      setAlertText("Successfully updated profile");
      setSeverity("success");
    } catch (error) {
      setAlertText(error.response.data.detail);
      setSeverity("error");
    }
    setOpen(true);
    setLoading(false);
  };

  const handlePasswordUpdate = async () => {
    dispatch(setPassword1(password1.value));
    dispatch(setPassword2(password2.value));
    if (password1.error || password2.error) {
      return;
    }
    setLoading(true);
    try {
      await axios.patch("/api/users/me", {
        username: username.value,
        password: password1.value,
      });
      setAlertText("Successfully updated password");
      setSeverity("success");
    } catch (error) {
      setAlertText(error.response.data.detail);
      setSeverity("error");
    }
    setOpen(true);
    setLoading(false);
  };

  const handleLinkCac = async () => {
    setLoading(true);
    try {
      await axios.put("/api/users/link-cac");
      setAlertText("Successfully linked CAC");
      setSeverity("success");
    } catch (error) {
      setAlertText(error.response.data.detail);
      setSeverity("error");
    }
    setOpen(true);
    setLoading(false);
  };

  const usernameFieldProps: TextFieldProps = {
    label: "Username",
    id: "username",
    disabled: true,
    ...username,
  };

  const fullNameFieldProps: TextFieldProps = {
    label: "Full Name",
    name: "fullName",
    id: "fullName",
    onChange: handleChange,
    ...fullName,
  };

  const password1FieldProps: TextFieldProps = {
    label: "New Password",
    type: "password",
    name: "password1",
    id: "password1",
    onChange: handleChange,
    ...password1,
  };

  const password2FieldProps: TextFieldProps = {
    label: "Confirm Password",
    type: "password",
    name: "password2",
    id: "password2",
    onChange: handleChange,
    ...password2,
  };

  return (
    <>
      <Grid item>
        <TextField {...usernameFieldProps} />
      </Grid>
      <Grid item>
        <TextField {...fullNameFieldProps} />
      </Grid>
      <Grid item>
        <Button
          variant="contained"
          onClick={handleProfileUpdate}
          disabled={loading}
        >
          Update Profile
          {loading && <CircularProgress size={25} />}
        </Button>
      </Grid>
      <Grid item>
        <TextField {...password1FieldProps} />
      </Grid>
      <Grid item>
        <TextField {...password2FieldProps} />
      </Grid>
      <Grid item>
        <Button
          variant="contained"
          onClick={handlePasswordUpdate}
          disabled={loading}
        >
          Change Password
          {loading && <CircularProgress size={25} />}
        </Button>
      </Grid>
      <Grid item>
        <Button variant="contained" onClick={handleLinkCac} disabled={loading}>
          Link CAC
          {loading && <CircularProgress size={25} />}
        </Button>
      </Grid>
    </>
  );
};

export default ViewProfile;
