import { Grid, makeStyles, Toolbar } from "@material-ui/core";
import { Switch, Route } from "react-router-dom";
import React, { useContext } from "react";
import Login from "components/auth/Login";
import Logout from "components/auth/Logout";
import Register from "components/auth/Register";
import SessionContext from "SessionContext";
import Home from "components/Home";
import ViewUsers from "components/admin/ViewUsers";
import UpdateUser from "components/admin/UpdateUser";
import ViewProfile from "components/ViewProfile";
import NotFound from "components/NotFound";

const useStyles = makeStyles((theme) => ({
  content: {
    flexGrow: 1,
    passing: theme.spacing(3),
  },
}));

const Main: React.FC = () => {
  const { user } = useContext(SessionContext);
  const classes = useStyles();

  var routes = [
    <Route exact path="/logout">
      <Logout />
    </Route>,
  ];
  if (user.id) {
    if (user.isSuperUser) {
      routes.push(
        <Route exact path="/admin">
          <ViewUsers />
        </Route>
      );
      routes.push(
        <Route exact path="/admin/users/:user">
          <UpdateUser />
        </Route>
      );
    }
    routes.push(
      <Route exact path="/profile">
        <ViewProfile />
      </Route>
    );
    routes.push(
      <Route exact path="/">
        <Home />
      </Route>
    );
    routes.push(
      <Route path="/">
        <NotFound />
      </Route>
    );
  } else {
    routes.push(
      <Route exact path="/register">
        <Register />
      </Route>
    );
    routes.push(
      <Route path="/">
        <Login />
      </Route>
    );
  }

  return (
    <main className={classes.content}>
      <Grid
        container
        direction="column"
        justify="space-around"
        alignItems="center"
        spacing={3}
      >
        <Grid item>
          <Toolbar />
        </Grid>
        <Switch>{routes}</Switch>
      </Grid>
    </main>
  );
};

export default Main;
