import React from "react";
import { render } from "@testing-library/react";
import Main from ".";
import SessionContext from "SessionContext";
import { MemoryRouter } from "react-router-dom";

type User = {
  id: number;
  isSuperUser: boolean;
};

const loggedOutUser = { id: 0, isSuperUser: false };
const normalUser = { id: 1, isSuperUser: false };
const superuser = { id: 1, isSuperUser: true };

const mockRoute = (path: string) =>
  jest.mock(path, () => {
    return {
      __esModule: true,
      default: () => <div>{path.split("/")[-1]}</div>,
    };
  });

mockRoute("components/auth/Login");
mockRoute("components/auth/Register");
mockRoute("components/auth/Logout");
mockRoute("components/NotFound");
mockRoute("components/Home");
mockRoute("components/admin/ViewUsers");
mockRoute("components/admin/UpdateUser");
mockRoute("components/ViewProfile");

const alwaysAvailableRoutes = [{ route: "/logout", text: "Logout" }];
const loggedOutRoutes = [
  { route: "/login", text: "Login" },
  { route: "/register", text: "Register" },
];
const loggedInRoutes = [
  { route: "", text: "Home" },
  { route: "bad-route", text: "NotFound" },
  { route: "/profile", text: "ViewProfile" },
];
const superuserRoutes = [
  { route: "/admin", text: "ViewUsers" },
  { route: "/admin/users/1", text: "UpdateUser" },
];

const renderWrappedComponent = (user: User, route: string) => {
  return render(
    <SessionContext.Provider
      value={{
        user,
        setUser: () => {},
        syncLogout: () => {},
        refresh: () => {},
      }}
    >
      <MemoryRouter initialEntries={[route]}>
        <Main />
      </MemoryRouter>
    </SessionContext.Provider>
  );
};

describe("<Main />", () => {
  it("should have render all applicable routes for logged out users", () => {
    for (const route of alwaysAvailableRoutes.concat(loggedOutRoutes)) {
      const { queryByText } = renderWrappedComponent(
        loggedOutUser,
        route.route
      );
      expect(queryByText(route.text)).toBeInTheDocument;
    }
  });
  it("should NOT render logged in or admin routes for logged out users", () => {
    for (const route of loggedInRoutes.concat(superuserRoutes)) {
      const { queryByText } = renderWrappedComponent(
        loggedOutUser,
        route.route
      );
      expect(queryByText(route.text)).not.toBeInTheDocument;
    }
  });
  it("should render all applicable routes for logged in users", () => {
    for (const route of alwaysAvailableRoutes.concat(loggedInRoutes)) {
      const { queryByText } = renderWrappedComponent(normalUser, route.route);
      expect(queryByText(route.text)).toBeInTheDocument;
    }
  });
  it("should not render logged out or admin routes for normal logged in users", () => {
    for (const route of loggedOutRoutes.concat(superuserRoutes)) {
      const { queryByText } = renderWrappedComponent(normalUser, route.route);
      expect(queryByText(route.text)).not.toBeInTheDocument;
    }
  });
  it("should render all applicable routes for logged in superusers", () => {
    for (const route of alwaysAvailableRoutes.concat(loggedInRoutes).concat(superuserRoutes)) {
      const { queryByText } = renderWrappedComponent(superuser, route.route);
      expect(queryByText(route.text)).toBeInTheDocument;
    }
  });
  it("should not render logged out routes for normal superusers", () => {
    for (const route of loggedOutRoutes) {
      const { queryByText } = renderWrappedComponent(superuser, route.route);
      expect(queryByText(route.text)).not.toBeInTheDocument;
    }
  });
});
