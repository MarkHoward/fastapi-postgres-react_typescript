import React from "react";
import { render } from "@testing-library/react";
import Sidebar from ".";
import { MemoryRouter } from "react-router-dom";
import SessionContext from "SessionContext";

type User = {
  id: number;
  isSuperUser: boolean;
};

const loggedOutUser = { id: 0, isSuperUser: false };
const normalUser = { id: 1, isSuperUser: false };
const superuser = { id: 1, isSuperUser: true };

const loggedOutRoutes = ["Login", "Register"];
const loggedInRoutes = ["Profile", "Logout"];
const superuserRoutes = ["Admin"];

const renderWrappedComponent = (user: User) => {
  return render(
    <SessionContext.Provider
      value={{
        user,
        setUser: () => {},
        syncLogout: () => {},
        refresh: () => {},
      }}
    >
      <MemoryRouter>
        <Sidebar />
      </MemoryRouter>
    </SessionContext.Provider>
  );
};

describe("<Sidebar />", () => {
  it("should have links for routes available to logged out users", () => {
    const { queryByText } = renderWrappedComponent(loggedOutUser);
    for (const route of loggedOutRoutes) {
      expect(queryByText(route)).toBeInTheDocument;
    }
  });
  it("should NOT have links for routes requiring login for logged out users", () => {
    const { queryByText } = renderWrappedComponent(loggedOutUser);
    for (const route of loggedInRoutes.concat(superuserRoutes)) {
      expect(queryByText(route)).not.toBeInTheDocument;
    }
  });
  it("should have links for routes available to logged in users", () => {
    const { queryByText } = renderWrappedComponent(normalUser);
    for (const route of loggedInRoutes) {
      expect(queryByText(route)).toBeInTheDocument;
    }
  });
  it("should NOT have links for logged out users or superusers for normal users", () => {
    const { queryByText } = renderWrappedComponent(normalUser);
    for (const route of loggedOutRoutes.concat(superuserRoutes)) {
      expect(queryByText(route)).not.toBeInTheDocument;
    }
  });
  it("should have links for logged in users and superusers for superusers", () => {
    const { queryByText } = renderWrappedComponent(superuser);
    for (const route of loggedInRoutes.concat(superuserRoutes)) {
      expect(queryByText(route)).toBeInTheDocument;
    }
  });
  it("should NOT have links for logged out users for superusers", () => {
    const { queryByText } = renderWrappedComponent(superuser);
    for (const route of loggedOutRoutes) {
      expect(queryByText(route)).not.toBeInTheDocument;
    }
  });
});
