import {
  Divider,
  Drawer,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  makeStyles,
  Toolbar,
} from "@material-ui/core";
import React, { forwardRef, useMemo, useContext, Ref } from "react";
import { Link as RouterLink } from "react-router-dom";
import SessionContext from "SessionContext";

const drawerWidth = 200;

const useStyles = makeStyles(() => ({
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerContainer: {
    overflow: "auto",
  },
}));

interface ListItemLinkProps {
  icon?: string;
  primary: string;
  to: string;
}

const ListItemLink = (props: ListItemLinkProps) => {
  const { icon, primary, to } = props;

  const renderLink = useMemo(
    () =>
      forwardRef((itemProps, ref: Ref<HTMLAnchorElement>) => (
        <RouterLink to={to} ref={ref} {...itemProps} />
      )),
    [to]
  );

  return (
    <li>
      <ListItem button component={renderLink}>
        {icon && <ListItemIcon>{icon}</ListItemIcon>}
        <ListItemText primary={primary} />
      </ListItem>
    </li>
  );
};

const Sidebar: React.FC = () => {
  const classes = useStyles();
  const { user } = useContext(SessionContext);

  var links = [];
  if (user.id) {
    links.push(<ListItemLink to="/profile" primary="Profile" />);
    links.push(<ListItemLink to="/logout" primary="Logout" />);
    if (user.isSuperUser) {
      links.push(<Divider />);
      links.push(<ListItemLink to="/admin" primary="Admin" />);
    }
  } else {
    links.push(<ListItemLink to="/login" primary="Login" />);
    links.push(<ListItemLink to="/register" primary="Register" />);
  }

  return (
    <Drawer
      variant="permanent"
      className={classes.drawer}
      classes={{ paper: classes.drawerPaper }}
    >
      <Toolbar />
      <div className={classes.drawerContainer}>
        <List>{links}</List>
      </div>
    </Drawer>
  );
};

export default Sidebar;
